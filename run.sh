#!/bin/bash

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" &> /dev/null && pwd)

docker run -v $SCRIPT_DIR/nwm.20220409_short_range_nwm.t00z.short_range.channel_rt.f001.conus.nc:/test.nc -t docker.io/netcdf_extractor/latest python main.py
