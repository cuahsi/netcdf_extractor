import json

import netCDF4

import nc_meta
import nc_utils


def add_original_coverage_metadata(metadata_list, extracted_metadata):
    """
    Adds data for the original coverage element to the *metadata_list*
    :param metadata_list: list to  which original coverage data needs to be added
    :param extracted_metadata: a dict containing netcdf extracted metadata
    :return:
    """

    ori_cov = {}
    if extracted_metadata.get('original-box'):
        coverage_data = extracted_metadata['original-box']
        projection_string_type = ""
        projection_string_text = ""
        datum = ""
        if extracted_metadata.get('projection-info'):
            projection_string_type = extracted_metadata[
                'projection-info']['type']
            projection_string_text = extracted_metadata[
                'projection-info']['text']
            datum = extracted_metadata['projection-info']['datum']

        ori_cov = {'originalcoverage':
                   {'value': coverage_data,
                    'projection_string_type': projection_string_type,
                    'projection_string_text': projection_string_text,
                    'datum': datum
                    }
                   }
    if ori_cov:
        metadata_list.append(ori_cov)


def add_variable_metadata(metadata_list, extracted_metadata):
    """
    Adds variable(s) related data to the *metadata_list*
    :param metadata_list: list to  which variable data needs to be added
    :param extracted_metadata: a dict containing netcdf extracted metadata
    :return:
    """
    for var_name, var_meta in list(extracted_metadata.items()):
        meta_info = {}
        for element, value in list(var_meta.items()):
            if value != '':
                meta_info[element] = value
        metadata_list.append({'variable': meta_info})


def add_spatial_coverage_metadata(metadata_list, extracted_metadata):
    """
    Adds data for one spatial coverage metadata element to the *metadata_list**
    :param metadata_list: list to which spatial coverage data needs to be added
    :param extracted_metadata: a dict containing netcdf extracted metadata
    :return:
    """
    if extracted_metadata.get('box'):
        box = {'coverage': {'type': 'box', 'value': extracted_metadata['box']}}
        metadata_list.append(box)


def add_temporal_coverage_metadata(metadata_list, extracted_metadata):
    """
    Adds data for one temporal metadata element to the *metadata_list*
    :param metadata_list: list to which temporal coverage data needs to be added
    :param extracted_metadata: a dict containing netcdf extracted metadata
    :return:
    """
    if extracted_metadata.get('period'):
        period = {
            'coverage': {'type': 'period', 'value': extracted_metadata['period']}}
        metadata_list.append(period)


def add_keywords_metadata(metadata_list, extracted_metadata, file_type=True):
    """
    Adds data for subject/keywords element to the *metadata_list*
    :param metadata_list: list to which keyword data needs to be added
    :param extracted_metadata: a dict containing netcdf extracted metadata
    :param file_type: If True then this metadata extraction is for netCDF file type, otherwise
    metadata extraction is for NetCDF resource
    :return:
    """
    if extracted_metadata.get('subject'):
        keywords = extracted_metadata['subject'].split(',')
        if file_type:
            metadata_list.append({'subject': keywords})
        else:
            for keyword in keywords:
                metadata_list.append({'subject': {'value': keyword}})

def add_metadata_to_list(extracted_core_meta, extracted_specific_meta,
                         file_meta_list):
    """
    Helper function to populate metadata lists (*res_meta_list* and *file_meta_list*) with
    extracted metadata from the NetCDF file. These metadata lists are then used for creating
    metadata element objects by the caller.
    :param res_meta_list: a list to store data to create metadata elements at the resource level
    :param extracted_core_meta: a dict of extracted dublin core metadata
    :param extracted_specific_meta: a dict of extracted metadata that is NetCDF specific
    :param file_meta_list: a list to store data to create metadata elements at the file type level
    (must be None when this helper function is used for NetCDF resource and must not be None
    when used for NetCDF file type
    :param resource: an instance of BaseResource (must be None when this helper function is used
    for NteCDF resource and must not be None when used for NetCDF file type)
    :return:
    """

    # add title

    # add abstract (Description element)

    # add keywords
    if file_meta_list is not None:
        # file type
        add_keywords_metadata(file_meta_list, extracted_core_meta)

    # add creators:

    # add contributors:

    # add relation of type 'source' (applies only to NetCDF resource type)

    # add relation of type 'references' (applies only to NetCDF resource type)

    # add rights (applies only to NetCDF resource type)

    # add coverage - period
    if file_meta_list is not None:
        # file type
        add_temporal_coverage_metadata(file_meta_list, extracted_core_meta)

    # add coverage - box
    if file_meta_list is not None:
        # file type
        add_spatial_coverage_metadata(file_meta_list, extracted_core_meta)

    # add variables
    if file_meta_list is not None:
        # file type
        add_variable_metadata(file_meta_list, extracted_specific_meta)

    # add original spatial coverage
    if file_meta_list is not None:
        # file type
        add_original_coverage_metadata(file_meta_list, extracted_core_meta)


file_type_metadata = []

filename = "test.nc"

# file validation and metadata extraction
res_dublin_core_meta, res_type_specific_meta = nc_meta.get_nc_meta_dict(filename)
add_metadata_to_list(res_dublin_core_meta, res_type_specific_meta, file_type_metadata)
metadata_dict = {}
# use the extracted metadata to populate file metadata
for element in file_type_metadata:
    # here k is the name of the element
    # v is a dict of all element attributes/field names and field values
    k, v = list(element.items())[0]
    metadata_dict[k] = v
print(json.dumps(metadata_dict, indent=2))
