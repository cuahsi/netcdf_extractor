FROM hydroshare/hs_docker_base:release-1.13

COPY main.py main.py
COPY nc_meta.py nc_meta.py
COPY nc_utils.py nc_utils.py
#COPY nwm.20220409_short_range_nwm.t00z.short_range.channel_rt.f001.conus.nc test.nc

#CMD [ "python", "./main.py"]